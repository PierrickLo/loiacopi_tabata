package com.example.loiacopi_tabata.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.loiacopi_tabata.model.Seance;

import java.util.List;

@Dao
public interface SeanceDao {
        @Query("SELECT * FROM seance")
        List<Seance> getAll();

        @Insert
        void insert(Seance seance);



        @Delete
        void deleteSeance(Seance... seance);

        @Update
        void update(Seance seance);
}
