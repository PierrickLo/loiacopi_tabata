package com.example.loiacopi_tabata.adapters;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.loiacopi_tabata.R;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ///////// SEULS LES SORCIERS VENANT DE POUDLARD POURRONT LIRE CES LIGNES /////////

    private LinearLayout seancesData;
    TextView name, txtTempsSeance;
    private ImageButton delete, edit;

    private SeanceClickListener seanceClickListener;
    private SeanceEditClickListener seanceEditClickListener;
    private SeanceDeleteClickListener seanceDeleteClickListener;

    ViewHolder(View itemView, SeanceClickListener seanceClickListener, SeanceEditClickListener seanceEditClickListener, SeanceDeleteClickListener seanceDeleteClickListener) {
        super(itemView);
        seancesData =  itemView.findViewById(R.id.seanceLigne);

        this.seanceClickListener = seanceClickListener;
        this.seanceDeleteClickListener = seanceDeleteClickListener;
        this.seanceEditClickListener = seanceEditClickListener;

        name = (TextView) itemView.findViewById(R.id.txtNomSeance);
        txtTempsSeance = (TextView) itemView.findViewById(R.id.txtTempsSeance);
        delete = (ImageButton) itemView.findViewById(R.id.btnListeSuppr);
        edit = (ImageButton) itemView.findViewById(R.id.btnListeEdit);

        seancesData.setOnClickListener(this);
        edit.setOnClickListener(monEdit);
        delete.setOnClickListener(deleteSeance);
    }

    private View.OnClickListener monEdit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            seanceEditClickListener.onEditSeanceClick(getAdapterPosition());
        }
    };

    private View.OnClickListener deleteSeance = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            seanceDeleteClickListener.onSeanceDeleteClick(getAdapterPosition());
        }
    };

    @Override
    public void onClick(View view) {
        seanceClickListener.onSeanceClick(getAdapterPosition());
    }

    /*@Override
    public boolean onLongClick(View v) {
        seanceDeleteClickListener.onSeanceDeleteClick(getAdapterPosition());
        return true;
    }*/

    public interface SeanceClickListener{
        void onSeanceClick(int position);
    }

    public interface SeanceEditClickListener{
        void onEditSeanceClick(int position);
    }

    public interface SeanceDeleteClickListener{
        void onSeanceDeleteClick(int position);
    }






}
