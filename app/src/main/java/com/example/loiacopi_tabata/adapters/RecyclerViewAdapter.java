package com.example.loiacopi_tabata.adapters;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.loiacopi_tabata.R;
import com.example.loiacopi_tabata.SeanceActivity;
import com.example.loiacopi_tabata.model.Seance;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    ///////// SEULS LES SORCIERS VENANT DE POUDLARD POURRONT LIRE CES LIGNES /////////

    private List<Seance> seanceList;
    private ViewHolder.SeanceClickListener mOnSeanceListener;
    private ViewHolder.SeanceEditClickListener mOnSeanceEditListener;
    private ViewHolder.SeanceDeleteClickListener mOnSeanceDeleteListener;


    public RecyclerViewAdapter(List<Seance> seanceList, ViewHolder.SeanceClickListener onSeanceListener, ViewHolder.SeanceEditClickListener onEditSeanceClick, ViewHolder.SeanceDeleteClickListener onSeanceDeleteListener){
        this.seanceList = seanceList;
        this.mOnSeanceListener = onSeanceListener;
        this.mOnSeanceEditListener = onEditSeanceClick;
        this.mOnSeanceDeleteListener = onSeanceDeleteListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_liste_seance, parent, false);
        return new ViewHolder(itemView, mOnSeanceListener,mOnSeanceEditListener, mOnSeanceDeleteListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Seance seance = seanceList.get(position);
        holder.name.setText(seance.getNom());
        holder.txtTempsSeance.setText(seance.getDureeTotal());
    }

    @Override
    public int getItemCount() {
        return seanceList.size();
    }

    public Seance getItem(int id){
        return this.seanceList.get(id);
    }

    public List<Seance> getItems(){
        return this.seanceList;
    }

}
