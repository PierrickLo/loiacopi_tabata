package com.example.loiacopi_tabata;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.media.MediaPlayer;


import com.example.loiacopi_tabata.model.Seance;

import java.util.ArrayList;

public class SeanceActivity extends AppCompatActivity {

    // VARIABLES
    private Seance seance;
    private CountDownTimer timer;
    private ArrayList<String> tabataCycle;
    private int nbrEtape = 0;
    private long updatedTime;
    private boolean pauseOn = false;
    private boolean quitterSeanceTerminee = false;

    // VIEWS
    private MediaPlayer mediaPlayer;
    private RelativeLayout seance_activity;
    private Button startPauseButton;
    private TextView timerValue;
    private TextView nameStep;
    private ImageView img_opacity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seance);
        seance = getIntent().getParcelableExtra("SEANCE"); //Récupération de l'instance de Seance
        tabataCycle = seance.getTabataCycle(); // On alimente une ArrayList avec chaque étape de la séance dans l'ordre

        seance_activity = (RelativeLayout) findViewById(R.id.seance_activity);
        img_opacity = (ImageView) findViewById(R.id.img_opacity);
        timerValue = (TextView) findViewById(R.id.timerValue);
        nameStep = (TextView) findViewById(R.id.nameStep);
        startPauseButton = (Button) findViewById(R.id.startPauseButton);

        if (savedInstanceState != null){
            stopPlayer(); // Dans le doute, on arrete les sons
            updatedTime = savedInstanceState.getLong("tempsTimerRestant");
            nbrEtape = savedInstanceState.getInt("etapeIndex");
            pauseOn = savedInstanceState.getBoolean("pauseOn");

            setTextAndColor(tabataCycle.get(nbrEtape-1));

            this.startTimer();

            if(!pauseOn){
                timer.cancel();
                update();
                pauseOn = false;
                if (!quitterSeanceTerminee){
                    startPauseButton.setText("GO");
                }
            }
        } else {
            update(); //Methode de mise à jour du timer
            nextStep(); // Methode permettant de passer à l'étape suivante, en commencant par la préparation au lancement de l'activité
        }

    }

    // Méthode pour lancer le temps de préparation
    private void startPreparation(){
        setTextAndColor(tabataCycle.get(nbrEtape));
        updatedTime = seance.getPreparation() * 1000;
        this.startTimer();
    }

    // Méthode pour lancer le temps de l'exercice
    private void startExercice() {
        lancerSon("work");

        setTextAndColor(tabataCycle.get(nbrEtape));
        updatedTime = seance.getTempsExercices() * 1000;
        this.startTimer();
    }

    // Méthode pour lancer le temps de repos
    private void startRepos() {
        lancerSon("rest");

        setTextAndColor(tabataCycle.get(nbrEtape));
        updatedTime = seance.getRepos() * 1000;
        this.startTimer();
    }

    // Méthode pour lancer le temps de repos long
    private void startReposLong() {
        lancerSon("rest");
        setTextAndColor(tabataCycle.get(nbrEtape));
        updatedTime = seance.getReposLong() * 1000;
        this.startTimer();
    }

    // Methode permettant de passer à l'étape suivante
    // Cette méthode s'appuit sur l'arraylist des étapes, elle est lancée à la fin de chaque timer pour déterminer quel sera le prochain timer lancé
    private void nextStep() {
        if(tabataCycle.size() > nbrEtape) {
            switch (tabataCycle.get(nbrEtape)) {
                case "preparation":
                    startPreparation();
                    break;
                case "tempsExercices":
                    startExercice();
                    break;
                case "repos":
                    startRepos();
                    break;
                case "reposLong":
                    startReposLong();
                    break;
                default:
                    break;
            }
            nbrEtape++;
        } else {
            nameStep.setText("Séance terminée !");
            img_opacity.setBackgroundColor(Color.parseColor("#700037FF"));
            startPauseButton.setTextColor(Color.parseColor("#700037FF"));
            seance_activity.setBackgroundResource(R.drawable.fitness5);
            quitterSeanceTerminee = true;
            startPauseButton.setText("QUITTER");
        }
    }

    // Met à jour le temps du timer
    private void update() {
        // Décompositions en secondes et minutes
        int secs = (int) (updatedTime / 1000);
        int mins = secs / 60;
        secs = secs % 60;
        // Affichage du "timer"
        timerValue.setText("" + mins + ":" + String.format("%02d", secs));
    }

    // Lance le timer
    private void startTimer() {
        timer = new CountDownTimer(updatedTime, 10) {

            public void onTick(long millisUntilFinished) {
                pauseOn= true;
                updatedTime = millisUntilFinished;
                update();
            }

            public void onFinish() {
                pauseOn = false;
                updatedTime = 0;
                update();
                nextStep();
            }
        }.start();
    }



    private void setTextAndColor(String nomEtape){
        switch (nomEtape) {
            case "preparation":
                nameStep.setText("Preparation");
                img_opacity.setBackgroundColor(Color.parseColor("#700037FF"));
                startPauseButton.setTextColor(Color.parseColor("#700037FF"));
                seance_activity.setBackgroundResource(R.drawable.fitness2);
                break;
            case "tempsExercices":
                nameStep.setText("Au travail !");
                img_opacity.setBackgroundColor(Color.parseColor("#ABFF4E4E"));
                startPauseButton.setTextColor(Color.parseColor("#ABFF4E4E"));
                seance_activity.setBackgroundResource(R.drawable.fitness1);
                break;
            case "repos":
                nameStep.setText("Repos");
                img_opacity.setBackgroundColor(Color.parseColor("#700037FF"));
                startPauseButton.setTextColor(Color.parseColor("#700037FF"));
                seance_activity.setBackgroundResource(R.drawable.fitness5);
                break;
            case "reposLong":
                nameStep.setText("Ça va reprendre !");
                img_opacity.setBackgroundColor(Color.parseColor("#700037FF"));
                startPauseButton.setTextColor(Color.parseColor("#700037FF"));
                seance_activity.setBackgroundResource(R.drawable.fitness);
                break;
            default:
                break;
        }
    }


    // Methode permettant de mettre en pause le timer et de le relancer
    public void onStartPause(View view){
        if(quitterSeanceTerminee){
            finish();
        } else if (pauseOn){
            stopPlayer();
            timer.cancel();
            pauseOn = false;
            startPauseButton.setText("GO");
        } else {
            startTimer();
            pauseOn = true;
            startPauseButton.setText("PAUSE");
        }
    }

    // Methode pour démarrer un son
    private void lancerSon(String id) {
        stopPlayer();
        this.mediaPlayer = MediaPlayer.create(
                this,
                this.getResources().getIdentifier(id, "raw", this.getPackageName())
        );
        // Listener sur le son, lorsque le son est terminé, on l'arrete, sinon il se repetera
        this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlayer();
            }
        });
        // On lance le son
        this.mediaPlayer.start();
    }
    // Methode pour stop un son
    private void stopPlayer(){
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
    }

    // Permet de quitter le timer et de détruire l'activité
    public void onStop(View view) {
        stopPlayer(); // Dans le doute, on arrete les sons
        timer.cancel(); // dans le doute, on cancel le timer   ////////////// 1/2 Comme ça on est sur que le timer ne continu pas après la destruction de l'activité, et les sons ne sont plus joués !
        timer = null; // dans le doute, on passe timer à null  ///// 2/2 Les sons continuaient de se jouer, du coup seul solution trouvée et de killer le timer
        finish();
    }

    // Méthode permettant de conserver les données
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // On garde en mémoire notre séance
        outState.putInt("etapeIndex", nbrEtape);
        outState.putLong("tempsTimerRestant", updatedTime);
        outState.putBoolean("pauseOn", pauseOn);
        stopPlayer(); // Dans le doute, on arrete les sons
        timer.cancel();
    }

    @Override
    public void onBackPressed() {
        stopPlayer(); // Dans le doute, on arrete les sons
        timer.cancel(); // dans le doute, on cancel le timer   ////////////// 1/2 Comme ça on est sur que le timer ne continu pas après la destruction de l'activité, et les sons ne sont plus joués !
        timer = null; // dans le doute, on passe timer à null  ///// 2/2 Les sons continuaient de se jouer, du coup seul solution trouvée et de killer le timer
        finish();
    }
}
