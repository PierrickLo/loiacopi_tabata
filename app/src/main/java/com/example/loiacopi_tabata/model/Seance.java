package com.example.loiacopi_tabata.model;



import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "seance")
public class Seance implements Parcelable {

        @PrimaryKey(autoGenerate = true)
        private int id;

        @ColumnInfo(name = "nom")
        private String nom;

        @ColumnInfo(name = "cycleSeance")
        private int cycleSeance;

        @ColumnInfo(name = "cycleExo")
        private int cycleExo;

        @ColumnInfo(name = "preparation")
        private int preparation;

        @ColumnInfo(name = "repos")
        private int repos;

        @ColumnInfo(name = "reposLong")
        private int reposLong;

        @ColumnInfo(name = "tempsExercices")
        private int tempsExercices;

        @Ignore
        private static final String[] etapeSeance = {"Temps Preparation","Nombre de seance" , "Nombre d'exo", "Temps Exercice", "Temps Repos", "Temps Repos Long"};

        @Ignore
        private ArrayList<String> tabataCycle;

        @Ignore
        public Seance(int preparation, int cycleSeance,  int cycleExo, int tempsExercices, int repos, int reposLong) {
                this.preparation = preparation;
                this.cycleSeance = cycleSeance;
                this.cycleExo = cycleExo;
                this.tempsExercices = tempsExercices;
                this.repos = repos;
                this.reposLong = reposLong;
                tabataCycle = new ArrayList<>();
        }

        public Seance() {
                this.nom = "Test";
                this.preparation = 10;
                this.cycleSeance = 1;
                this.cycleExo = 5;
                this.tempsExercices = 60;
                this.repos = 15;
                this.reposLong = 60;
                tabataCycle = new ArrayList<>();
        }


        protected Seance(Parcel in) {
                id = in.readInt();
                nom = in.readString();
                cycleSeance = in.readInt();
                cycleExo = in.readInt();
                preparation = in.readInt();
                repos = in.readInt();
                reposLong = in.readInt();
                tempsExercices = in.readInt();
                tabataCycle = in.createStringArrayList();
        }

        public static final Creator<Seance> CREATOR = new Creator<Seance>() {
                @Override
                public Seance createFromParcel(Parcel in) {
                        return new Seance(in);
                }

                @Override
                public Seance[] newArray(int size) {
                        return new Seance[size];
                }
        };

        public String[] getEtapeSeance() { return etapeSeance; }

        public int getId() {
            return id;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public int getCycleSeance() { return cycleSeance;}

        public void setCycleSeance(int cycleSeance) {this.cycleSeance = cycleSeance; }

        public int getCycleExo() {return cycleExo;}

        public void setCycleExo(int cycleExo) {this.cycleExo = cycleExo;}

        public int getPreparation() {return preparation;}

        public void setPreparation(int preparation) {this.preparation = preparation; }

        public int getRepos() {return repos; }

        public void setRepos(int repos) {this.repos = repos; }

        public int getReposLong() { return reposLong; }

        public void setReposLong(int reposLong) {this.reposLong = reposLong; }

        public int getTempsExercices() {return tempsExercices; }

        public void setTempsExercices(int tempsExercices) {this.tempsExercices = tempsExercices; }

        public void setId(int id) {this.id = id;}

        public ArrayList getTabataCycle() {
                this.tabataCycle.clear();
                this.tabataCycle.add("preparation");
                for(int tabata = 1; tabata <= this.getCycleSeance(); tabata++) {
                        for(int cycle = 1; cycle <= this.getCycleExo(); cycle++) {
                                this.tabataCycle.add("tempsExercices");
                                if(cycle != this.getCycleExo()) {
                                        this.tabataCycle.add("repos");
                                }
                        }
                        if(tabata != this.getCycleSeance()) {
                                this.tabataCycle.add("reposLong");
                        }
                }
                return this.tabataCycle;
        }

        public void ajoutTemps(String etape){

                if(etape.equals(etapeSeance[0]) && this.getPreparation() < 999) { this.setPreparation(this.preparation + 1); }
                else if(etape.equals(etapeSeance[1]) && this.getCycleSeance() < 999) { this.setCycleSeance(this.cycleSeance + 1); }
                else if(etape.equals(etapeSeance[2]) && this.getCycleExo() < 999) { this.setCycleExo(this.cycleExo + 1); }
                else if(etape.equals(etapeSeance[3]) && this.getTempsExercices() < 999) { this.setTempsExercices(this.tempsExercices + 1); }
                else if(etape.equals(etapeSeance[4]) && this.getRepos() < 999) { this.setRepos(this.repos + 1); }
                else if(etape.equals(etapeSeance[5]) && this.getReposLong() < 999) { this.setReposLong(this.reposLong + 1); }

        }
        public void retraitTemps(String etape){

                if(etape.equals(etapeSeance[0]) && this.getPreparation() > 1) { this.setPreparation(this.preparation - 1); }
                else if(etape.equals(etapeSeance[1]) && this.getCycleSeance() > 1) { this.setCycleSeance(this.cycleSeance - 1); }
                else if(etape.equals(etapeSeance[2]) && this.getCycleExo() > 1) { this.setCycleExo(this.cycleExo - 1); }
                else if(etape.equals(etapeSeance[3]) && this.getTempsExercices() > 1) { this.setTempsExercices(this.tempsExercices - 1); }
                else if(etape.equals(etapeSeance[4]) && this.getRepos() > 1) { this.setRepos(this.repos - 1); }
                else if(etape.equals(etapeSeance[5]) && this.getReposLong() > 1) { this.setReposLong(this.reposLong - 1); }

        }

        @Override
        public int describeContents() {
                return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(id);
                dest.writeString(nom);
                dest.writeInt(cycleSeance);
                dest.writeInt(cycleExo);
                dest.writeInt(preparation);
                dest.writeInt(repos);
                dest.writeInt(reposLong);
                dest.writeInt(tempsExercices);
                dest.writeStringList(tabataCycle);
        }

        public String getDureeTotal() {
                this.getTabataCycle();
                int duration = 0;
                for(String step : this.tabataCycle) {
                        System.out.println(step);
                       /* switch (step) {
                                case "tempsExercices" : duration += this.getTempsExercices();
                                        break;
                                case "repos" : duration += this.getRepos();
                                        break;
                                case "preparation" : duration += this.getPreparation();
                                        break;
                                case "reposLong" : duration += this.getReposLong();
                                        break;
                                default: break;
                        }*/
                        if(step.equals("tempsExercices")) { duration += this.getTempsExercices(); }
                        else if(step.equals("repos")) { duration += this.getRepos(); }
                        else if(step.equals("preparation")) { duration += this.getPreparation(); }
                        else if(step.equals("reposLong")) { duration += this.getReposLong(); }
                }
                int secs = duration;
                int mins = secs / 60;
                secs = secs % 60;

                return ("" + mins + ":" + String.format("%02d", secs));
        }
}
