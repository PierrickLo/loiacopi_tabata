package com.example.loiacopi_tabata;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.loiacopi_tabata.Libraries.RepeatListener;
import com.example.loiacopi_tabata.db.DatabaseClient;
import com.example.loiacopi_tabata.model.Seance;

public class MainActivity extends AppCompatActivity {

    // CONSTANTE
    static final String STATE_SEANCE = "SEANCE";

    // VAR
    private DatabaseClient mDb;
    private Seance seance;

    // VIEWS
    private String[] etapeTimer = {"preparation","cycleSeance" , "cycleExo", "tempsExercices", "repos", "reposLong"};
    private TextView[] afficherTempsTravail = new TextView[etapeTimer.length];
    private TextView txtEtape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDb = DatabaseClient.getInstance(getApplicationContext());

        if (savedInstanceState != null){
            // Récupération de l'instance
            seance = (Seance)savedInstanceState.getParcelable(STATE_SEANCE);
            initialisationActivity();
        } else {
            // Création de la séance
            seance = new Seance();
            initialisationActivity();
        }
    }

    // Lancement du timer
    public void onStart(View view) {
        // On passe la seance dans une nouvelle activité
        Intent intent = new Intent(this, SeanceActivity.class);
        intent.putExtra("SEANCE",this.seance);
        startActivity(intent);
    }

    // Accéder à l'activité contenant la liste de nos séances
    public void onListeSeance(View view) {
        // On lance l'activité contenant la liste des séance
        Intent intent = new Intent(this, ListeActivity.class);
        startActivity(intent);
    }

    // Ajout +1 au temps d'une étape
    public void onPlus(View view) {
        String etape = ((ImageButton)view).getTag().toString();
        seance.ajoutTemps(etape);
        update();
    }
    // Retire -1 au temps d'une étape
    public void onLess(View view) {
        String etape = ((ImageButton)view).getTag().toString();
        seance.retraitTemps(etape);
        update();
    }

    // Methode d'initialisation des données pour l'activité
    @SuppressLint("ClickableViewAccessibility")
    public void initialisationActivity() {
        int i = 0;
        for (String step : seance.getEtapeSeance()){
            LinearLayout linearTMP = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_liste_bouton, null);
            ImageButton less = (ImageButton) linearTMP.findViewById(R.id.btnMoins);
            afficherTempsTravail[i] = (TextView) linearTMP.findViewById(R.id.txtTemps);
            txtEtape = (TextView) linearTMP.findViewById(R.id.txtEtape);
            txtEtape.setText(step); // set du nom de l'étape
            ImageButton plus = (ImageButton) linearTMP.findViewById(R.id.btnPlus);
            less.setTag(step); // ajout d'un tag au bouton -
            plus.setTag(step); // ajout d'un tag au bouton +
            ((LinearLayout) findViewById(R.id.edit)).addView(linearTMP);
            i++;

            // Permet de maintenir le bouton "-" pour éditer le temps plus rapidement
            less.setOnTouchListener(new RepeatListener(400, 70, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLess(view);
                }
            }));
            // Permet de maintenir le bouton "+" pour éditer le temps plus rapidement
            plus.setOnTouchListener(new RepeatListener(400, 70, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPlus(view);
                }
            }));
        }
        // Initialisation de l'affichage avec les paramètres sauvegardé
        update();
    }

    // Methode de mise à jour des données d'affichage des temps des étapes
    public void update (){
        // On met à jour tous les TextView d'un coup, ça nous évite de faire plein de condition !
        // On peut se le permettre car ça demande pas de ressource
        afficherTempsTravail[0].setText(String.valueOf(seance.getPreparation()));
        afficherTempsTravail[1].setText(String.valueOf(seance.getCycleSeance()));
        afficherTempsTravail[2].setText(String.valueOf(seance.getCycleExo()));
        afficherTempsTravail[3].setText(String.valueOf(seance.getTempsExercices()));
        afficherTempsTravail[4].setText(String.valueOf(seance.getRepos()));
        afficherTempsTravail[5].setText(String.valueOf(seance.getReposLong()));
    }

    // Méthode permettant de conserver les données
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // On garde en mémoire notre séance
        outState.putParcelable(STATE_SEANCE, seance);
    }

    //Sauvegarde de la séance dans la base de données
    public void saveSeance(View view) {
        class SaveTask extends AsyncTask<Void, Void, Seance> {

            @Override
            protected Seance doInBackground(Void... voids) {
                // adding to database
                mDb.getAppDatabase()
                        .SeanceDao()
                        .insert(seance);

                return seance;
            }

            @Override
            protected void onPostExecute(Seance seance) {
                super.onPostExecute(seance);

                // Quand la tache est créée, on arrête l'activité SaveTask (on l'enleve de la pile d'activités)
                setResult(RESULT_OK);
                Toast.makeText(getApplicationContext(), "Enregistré", Toast.LENGTH_LONG).show();
                // On passe la chaine de caractère à la nouvelle activité
            }
        }

        SaveTask st = new SaveTask();
        st.execute(); // execution de la requete
    }

    // Méthode pour enregistrer la séance
    public void nouvelleSeance(View view) {

        // Boite de dialogue pour la saisie du nom de la séance
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompt.xml pour set le dialog
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set du message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Enregistrer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                seance.setNom(userInput.getText().toString());
                                saveSeance(null);
                            }
                        })
                .setNegativeButton("Annuler",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // création de l'alert
        AlertDialog alertDialog = alertDialogBuilder.create();
        // affichage du dialog
        alertDialog.show();
    }


    // Méthode qui Override le bouton retour du smartphone, ici je m'en sers pour demander à l'utilisateur s'il veut quitter l'application
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Fermeture de l'application")
                .setMessage("Êtes-vous sur de vouloir quitter ?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("Annuler", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
