package com.example.loiacopi_tabata;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.loiacopi_tabata.adapters.RecyclerViewAdapter;
import com.example.loiacopi_tabata.adapters.ViewHolder;
import com.example.loiacopi_tabata.db.DatabaseClient;
import com.example.loiacopi_tabata.model.Seance;

import java.util.ArrayList;
import java.util.List;

import static com.example.loiacopi_tabata.MainActivity.STATE_SEANCE;

public class ListeActivity extends AppCompatActivity implements ViewHolder.SeanceClickListener, ViewHolder.SeanceEditClickListener, ViewHolder.SeanceDeleteClickListener {

    private RelativeLayout liste_activity;
    private RecyclerViewAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private List<Seance> seanceList = new ArrayList<>();
    DatabaseClient mDb;
    private ListeActivity listeActivity = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);
        mDb = DatabaseClient.getInstance(getApplicationContext());

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler);
        mRecyclerView.setAdapter(null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        GetSeancesEnregistrees recupererSeances = new GetSeancesEnregistrees();
        recupererSeances.execute();
        
       /* int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            liste_activity.setBackgroundResource(R.drawable.fitness2);

        } else {
            liste_activity.setBackgroundResource(R.drawable.fitness8);
            // In portrait
        }*/

    }

    //// MISE A JOUR DES DONNEES DE LA LISTE DE SEANCE ////
    private void populateListeSeances(final List<Seance> seances) {
                mAdapter = new RecyclerViewAdapter(seanceList, this, this, this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRecyclerView.setAdapter(mAdapter);

                if (seances.size() > 0) {
                    for (Seance s : seances) {
                        seanceList.add(s);
                    }
                }
            }
        });
    }


    //////Classe asynchrone permettant de supprimer une séance et de mettre à jour le RecyclerView de l'activité
    class GetSeancesEnregistrees extends AsyncTask<Void, Void, List<Seance>> {

        @Override
        protected List<Seance> doInBackground(Void... voids) {
             List<Seance> seances = mDb.getAppDatabase()
                    .SeanceDao()
                    .getAll();
            populateListeSeances(seances);
            return seances;
        }

        ////////////Mettre à jour l'adapter avec la liste de taches
        @Override
        protected void onPostExecute(List<Seance> seances) {
            super.onPostExecute(seances);
        }
    }

    public void deleteSeance(View view, final Seance sean) {
        class DeleteSeance extends AsyncTask<Void, Void, Seance> {

            @Override
            protected Seance doInBackground(Void... voids) {
                mDb.getAppDatabase()
                        .SeanceDao()
                        .deleteSeance(sean);
                return null;
            }

            ////////////Mettre à jour l'adapter avec la liste de taches
            @Override
            protected void onPostExecute(Seance seance) {
                super.onPostExecute(seance);
                setResult(RESULT_OK);
            }
        }

        DeleteSeance deleteSeance = new DeleteSeance();
        deleteSeance.execute();
    }

    // Méthode permettant de lancer une séance depuis la liste
    @Override
    public void onSeanceClick(int position){
        Intent intent = new Intent(getBaseContext(), SeanceActivity.class);
        intent.putExtra(STATE_SEANCE, mAdapter.getItem(position));
        startActivity(intent);
    }

    // Méthode permettant de modifier une séance depuis la liste
    @Override
    public void onEditSeanceClick(int position) {
        Seance seance = mAdapter.getItem(position);
        Intent intent = new Intent(getBaseContext(), ModificationSeanceActivity.class);
        intent.putExtra("SEANCE", seance);
        startActivity(intent);
        finish();
    }

    // Methode permettant de supprimer une séance dans la liste
    @Override
    public void onSeanceDeleteClick(final int position) {

        // Permet d'afficher une alerte pour la suppression
        new AlertDialog.Builder(this)
                .setTitle("Suppression d'une séance")
                .setMessage("Êtes-vous sur de vouloir supprimer cette séance ?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSeance(null, mAdapter.getItem(position));
                        mAdapter.getItems().remove(position);
                        mAdapter.notifyItemRemoved(position);
                        mAdapter.notifyDataSetChanged();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton("Annuler", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void onQuitter(View view) {
        finish();
    }

    // Méthode qui Override le bouton retour du smartphone, ici je m'en sers pour détruire l'activité en cours
    @Override
    public void onBackPressed() {
        finish();
    }

}
