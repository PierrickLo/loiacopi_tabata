package com.example.loiacopi_tabata;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.loiacopi_tabata.Libraries.RepeatListener;
import com.example.loiacopi_tabata.db.DatabaseClient;
import com.example.loiacopi_tabata.model.Seance;

public class ModificationSeanceActivity extends AppCompatActivity {

    // CONSTANTE
    private static final String STATE_SEANCE = "SEANCE";

    // VAR
    private Seance seance;
    private DatabaseClient mDb; // variable Database
    private String[] etapeTimer = {"preparation","cycleSeance" , "cycleExo", "tempsExercices", "repos", "reposLong"};

    // VIEWS
    private TextView[] afficherTempsTravail = new TextView[etapeTimer.length]; // la taille du tableau est égal au nombre d'étape
    private TextView txtEtape;
    private EditText nomSeance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_seance);
        mDb = DatabaseClient.getInstance(getApplicationContext());
        nomSeance = (EditText)findViewById(R.id.nomSeance);

        if (savedInstanceState != null){
            // Récupération de l'instance
            seance = (Seance)savedInstanceState.getParcelable("seance");
            nomSeance.setText(seance.getNom());
            initialisationActivity();

        } else {
            // Création de la séance
            seance = getIntent().getParcelableExtra(STATE_SEANCE);
            nomSeance.setText(seance.getNom());
            initialisationActivity();
        }
    }

    // Permet d'ajouter +1 au temps d'une étape
    public void onPlus(View view) {
        String etape = ((ImageButton)view).getTag().toString();
        seance.ajoutTemps(etape); // Méthode de l'objet pour ajouter +1 au temps d'une étape
        update(); // Mise à jour de l'affichage
    }
    // Permet de retirer -1 au temps d'une étape
    public void onLess(View view) {
        String etape = ((ImageButton)view).getTag().toString();
        seance.retraitTemps(etape); // Méthode de l'objet pour retirer -1 au temps d'une étape
        update(); // Mise à jour de l'affichage
    }

    // Methode d'initialisation des données pour l'activité
    @SuppressLint("ClickableViewAccessibility")
    public void initialisationActivity() {
        int i = 0;

        for (String step : seance.getEtapeSeance()){
            LinearLayout linearTMP = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_liste_bouton, null);
            ImageButton less = (ImageButton) linearTMP.findViewById(R.id.btnMoins);
            afficherTempsTravail[i] = (TextView) linearTMP.findViewById(R.id.txtTemps);
            txtEtape = (TextView) linearTMP.findViewById(R.id.txtEtape);
            txtEtape.setText(step);
            ImageButton plus = (ImageButton) linearTMP.findViewById(R.id.btnPlus);
            less.setTag(step);
            plus.setTag(step);
            ((LinearLayout) findViewById(R.id.edit)).addView(linearTMP);
            i++;

            // Permet de maintenir le bouton "-" pour éditer le temps plus rapidement
            less.setOnTouchListener(new RepeatListener(400, 70, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLess(view);
                }
            }));
            // Permet de maintenir le bouton "+" pour éditer le temps plus rapidement
            plus.setOnTouchListener(new RepeatListener(400, 70, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPlus(view);
                }
            }));
        }
        // Initialisation de l'affichage avec les paramètres sauvegardé
        update();
    }

    // Methode de mise à jour des données d'affichage des temps des étapes
    public void update (){
        // On met à jour tous les TextView d'un coup, ça nous évite de faire plein de condition !
        // On peut se le permettre car ça demande pas de ressource
        afficherTempsTravail[0].setText(String.valueOf(seance.getPreparation()));
        afficherTempsTravail[1].setText(String.valueOf(seance.getCycleSeance()));
        afficherTempsTravail[2].setText(String.valueOf(seance.getCycleExo()));
        afficherTempsTravail[3].setText(String.valueOf(seance.getTempsExercices()));
        afficherTempsTravail[4].setText(String.valueOf(seance.getRepos()));
        afficherTempsTravail[5].setText(String.valueOf(seance.getReposLong()));
    }

    // Permet d'enregistrer les modifications de l'objet
    public void onEnregistrer(View view) {
        seance.setNom(nomSeance.getText().toString()); // On récupérer le nom saisi, même si c'est le même
        editSeance(null); // On edit la séance dans la base de données
        Intent intent = new Intent(this, ListeActivity.class); // On créer l'Intent pour retourner à la liste des activité, ça permet de la mettre à jour l'affichage de la liste
        finish();
        startActivity(intent); // On lance l'activité
    }

    // Methode pour mettre à jour la base de données
    public void editSeance(View view) {
        class EditSeance extends AsyncTask<Void, Void, Seance> {

            @Override
            protected Seance doInBackground(Void... voids) {
                // adding to database
                mDb.getAppDatabase()
                        .SeanceDao()
                        .update(seance);

                return seance;
            }

            @Override
            protected void onPostExecute(Seance seance) {
                super.onPostExecute(seance);
                // Quand la tache est créée, on arrête l'activité AddTaskActivity (on l'enleve de la pile d'activités)
                setResult(RESULT_OK);
                // On passe la chaine de caractère à la nouvelle activité
            }
        }

        EditSeance st = new EditSeance();
        st.execute();
    }

    // Quitte et détruit l'activité en cours sans mis à jour de la base de données et lance l'activité contenant la liste
    public void onQuitter(View view) {
        Intent intent = new Intent(this, ListeActivity.class);
        startActivity(intent);
        finish();
    }

    // Méthode permettant de conserver les données
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // On garde en mémoire notre séance
        outState.putParcelable("seance", seance);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ListeActivity.class);
        startActivity(intent);
        finish();
    }
}
